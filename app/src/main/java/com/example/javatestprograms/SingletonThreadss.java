package com.example.javatestprograms;

//Question 5. singletonClass
class ThreadSingleton implements Runnable
{

    @Override
    public void run() {
        for (int count = 0 ; count < 4 ; count++)
        {
            System.out.println(Thread.currentThread().getName() + " : " + CreateSingletonClasss.getInstance().hashCode());
        }
    }
}

public class SingletonThreads {
    public static void main(String[] args) {

        ThreadSingleton threadSingleton1 = new ThreadSingleton();
        ThreadSingleton threadSingleton2 = new ThreadSingleton();
        ThreadSingleton threadSingleton3 = new ThreadSingleton();

        Thread t1 = new Thread(threadSingleton1);
        Thread t2 = new Thread(threadSingleton2);
        Thread t3 = new Thread(threadSingleton3);

        t1.start();
        t2.start();
        t3.start();

    }
}
