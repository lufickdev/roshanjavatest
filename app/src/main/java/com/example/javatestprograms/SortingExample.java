package com.example.javatestprograms;

import java.util.ArrayList;
import java.util.Collections;
//Question 3
class MyMobile {
    String name;
    int ram;
    int price;

    public MyMobile(String name, int ram, int price) {
        this.name = name;
        this.ram = ram;
        this.price = price;
    }
}

public class SortingExample {

    public static void main(String[] args) {

        ArrayList<MyMobile> arrayList = new ArrayList<MyMobile>();
        arrayList.add(new MyMobile("Samsung", 4, 12000));
        arrayList.add(new MyMobile("Samsung", 6, 15000));
        arrayList.add(new MyMobile("MI", 4, 10000));
        arrayList.add(new MyMobile("MI", 6, 13000));


        System.out.println("***********");
        Collections.sort(arrayList, new MyMobileComparator());
        for (MyMobile my : arrayList) {
            System.out.println(my.name + " " + my.ram + " " + my.price);
        }


    }
}
