package com.example.javatestprograms;

import java.util.Comparator;

public class PhoneComparator  implements Comparator<Mobiles> {
    @Override
    public int compare(Mobiles mobiles1, Mobiles mobiles2) {
        if (mobiles1.phone_Name.equals(mobiles2.phone_Name)&&
            mobiles1.phone_ram==mobiles2.phone_ram &&
            mobiles1.phone_price==mobiles2.phone_price)
        {
            return 1;
        }
        return -1;
    }
}
