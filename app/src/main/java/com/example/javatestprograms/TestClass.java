package com.example.javatestprograms;

import android.annotation.TargetApi;
import android.os.Build;

import java.util.ArrayList;
import java.util.Objects;

//Question 1;
class Mobile {
    String name;
    String ram;

    public Mobile(String name, String ram) {
        this.name = name;
        this.ram = ram;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRam() {
        return ram;
    }

    public void setRam(String ram) {
        this.ram = ram;
    }

    @Override
    public String toString() {
        return "Mobile{" +
                "name='" + name + '\'' +
                ", ram='" + ram + '\'' +
                '}';
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Mobile mobile = (Mobile) o;
        return Objects.equals(name, mobile.name) &&
                Objects.equals(ram, mobile.ram);
    }

    @Override
    public int hashCode() {
        return 0;
    }
}

public class TestClass {
    public static void main(String args[]) {

        ArrayList<Mobile> mobileArrayList = new ArrayList<Mobile>();
        mobileArrayList.add(new Mobile("Samsung", "4gb"));
        mobileArrayList.add(new Mobile("MI", "8gb"));
        mobileArrayList.add(new Mobile("Samsung", "4gb"));


        ArrayList<Mobile> mobileArrayList2 = new ArrayList<Mobile>();
        for (Mobile mobileData : mobileArrayList) {
            if (!mobileArrayList2.contains(mobileData))
            {
                System.out.println(mobileData);
                mobileArrayList2.add(mobileData);
            }

        }
    }
}
