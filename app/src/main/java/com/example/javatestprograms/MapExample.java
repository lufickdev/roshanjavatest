package com.example.javatestprograms;

import java.util.HashMap;
import java.util.Map;
//Question 2
public class MapExample {
    public static void main(String[] args) {

        HashMap<String,String> countryHasMap = new HashMap<>();
        countryHasMap.put("IN","INDIA");
        countryHasMap.put("US","UNITED STATES");
        countryHasMap.put("GB","UNITED KINGDOM");
        countryHasMap.put("ES","SPAIN");


        for (Map.Entry<String,String> map:countryHasMap.entrySet()
             ) {
            System.out.println(map.getValue()+"("+map.getKey() +")");
        }

    }
}
