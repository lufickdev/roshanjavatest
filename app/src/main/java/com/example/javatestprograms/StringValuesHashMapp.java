package com.example.javatestprograms;

import java.util.HashMap;
import java.util.Map;

public class StringValuesHashMapp {
    public static void main(String[] args) {

        String countryCodes = "IN=INDIA,US=UNITED STATES,UK=UNITED KINGDOM,ES=SPAIN";
        String[] keyValuepairs = countryCodes.split(",");

        Map<String, String> map = new HashMap<>();
        for (String pair : keyValuepairs)
        {
            String[] entry = pair.split("=");
            map.put(entry[0].trim(), entry[1].trim());
        }

        for (Map.Entry<String, String> map1 : map.entrySet())
        {
            System.out.println(map1.getValue() + "(" + map1.getKey() + ")");
        }
    }
}
