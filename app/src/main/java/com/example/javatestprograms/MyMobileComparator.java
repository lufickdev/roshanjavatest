package com.example.javatestprograms;

import java.util.Comparator;

 public class MyMobileComparator  implements Comparator<MyMobile> {
     @Override
     public int compare(MyMobile myMobile1, MyMobile myMobile2) {

         if (myMobile1.name.equals(myMobile2.name)) {
             if (myMobile1.ram > myMobile2.ram) {
                 return 1;
             } else return -1;
         }
         if (myMobile1.ram == myMobile2.ram) {
             if (myMobile1.price > myMobile2.price) {
                 return 1;
             } else return -1;
         }

         return 0;
     }
 }