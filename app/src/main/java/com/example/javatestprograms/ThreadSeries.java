package com.example.javatestprograms;

//Question 6 : 3 Threads inc. counter
class ThreadTestDemo implements Runnable {
    public int PRINT_NUMBERS_UPTO = 12;
    static int number = 1;
    int remainder;
    static Object lock = new Object();

    ThreadTestDemo(int remainder) {
        this.remainder = remainder;
    }

    @Override
    public void run() {
        while (number < PRINT_NUMBERS_UPTO - 1) {
            synchronized (lock) {
                while (number % 3 != remainder) { // wait for numbers other than remainder
                    try {
                        lock.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                System.out.println(Thread.currentThread().getName() + " " + number);
                number++;
                lock.notifyAll();
            }
        }
    }
}

public class ThreadSeries {
    public static void main(String[] args) {

        ThreadTestDemo runnable1 = new ThreadTestDemo(1);
        ThreadTestDemo runnable2 = new ThreadTestDemo(2);
        ThreadTestDemo runnable3 = new ThreadTestDemo(0);

        Thread t1 = new Thread(runnable1, "F_Thread");
        Thread t2 = new Thread(runnable2, "S_Thread");
        Thread t3 = new Thread(runnable3, "T_Thread");

        t1.start();
        t2.start();
        t3.start();
    }
}
