package com.example.javatestprograms;

import android.annotation.TargetApi;
import android.os.Build;

import java.util.ArrayList;
import java.util.Objects;

//Question 4 Remove duplicacy from the arraylist.

class Phones {
    String phoneName;
    int phoneRam;
    int phoneprice;

    public Phones(String phoneName, int phoneRam, int phoneprice) {
        this.phoneName = phoneName;
        this.phoneRam = phoneRam;
        this.phoneprice = phoneprice;
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Phones phones = (Phones) o;
        return phoneRam == phones.phoneRam &&
                phoneprice == phones.phoneprice &&
                Objects.equals(phoneName, phones.phoneName);
    }

    @Override
    public int hashCode() {
        return 0;
    }

    @Override
    public String toString() {
        return "" +
                "Phone:" + phoneName +
                "||Ram:" + phoneRam + "GB" +
                "||Price(INR):" + phoneprice;
    }
}

public class RemovedDuplicacy {
    public static void main(String[] args) {
        ArrayList<Phones> phones = new ArrayList<>();
        phones.add(new Phones("Apple", 4, 50000));
        phones.add(new Phones("Samsung", 6, 15000));
        phones.add(new Phones("Apple", 4, 50000));
        phones.add(new Phones("Apple", 4, 50000));
        phones.add(new Phones("Mi", 6, 13000));
        phones.add(new Phones("Mi", 6, 13000));
        phones.add(new Phones("Mi", 8, 17000));

        ArrayList<Phones> list2 = new ArrayList<Phones>();
        for (Phones element : phones) {
            if (!list2.contains(element)) {
                System.out.println(element);
                list2.add(element);
            }
        }

    }
}
