package com.example.javatestprograms;

import java.util.ArrayList;
import java.util.Collections;

//Sorting the Arraylist by ram and price.
class Mobiles {
    String phone_Name;
    int phone_ram;
    int phone_price;

    public Mobiles(String phone_Name, int phone_ram, int phone_price) {
        this.phone_Name = phone_Name;
        this.phone_ram = phone_ram;
        this.phone_price = phone_price;
    }
}

public class Sorting {
    public static void main(String[] args) {

        ArrayList<Mobiles> mobilesList = new ArrayList<>();
        mobilesList.add(new Mobiles("Samsung", 4, 12000));
        mobilesList.add(new Mobiles("Samsung", 6, 15000));
        mobilesList.add(new Mobiles("Mi", 4, 10000));
        mobilesList.add(new Mobiles("Mi", 6, 13000));

        System.out.println("List of Phones :: ");
        for (Mobiles mobileData : mobilesList) {
            System.out.println("Product: " + mobileData.phone_Name + " RAM : " + mobileData.phone_ram + "GB" + "  Price(INR): " + mobileData.phone_price);
        }

        System.out.println("\n*************/After Sorting/***************");
        Collections.sort(mobilesList, new PhoneComparator());
        for (Mobiles mobileData : mobilesList) {
            System.out.println("Product: " + mobileData.phone_Name + " RAM : " + mobileData.phone_ram + "GB" + "  Price(INR): " + mobileData.phone_price);
        }

    }
}
