package com.example.javatestprograms;

import java.util.ArrayList;

//Question No.1 store unique values into unique ArrayList.
class Mobile {
    String mobile_name;
    String mobile_ram;

    public Mobile(String mobile_name, String mobile_ram) {
        this.mobile_name = mobile_name;
        this.mobile_ram = mobile_ram;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj != null && obj instanceof Mobile) {
            String phoneName = ((Mobile) obj).mobile_name;
            String phoneRam = ((Mobile) obj).mobile_ram;
            if (phoneName != null && phoneName.equals(this.mobile_name) && phoneRam.equals(this.mobile_ram)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int hashCode() {
        return 0;
    }
}


public class UniqueArrayList {

    public static void main(String[] args) {

        ArrayList<Mobile> mobileList = new ArrayList<Mobile>();
        mobileList.add(new Mobile("Samsung", "4GB"));
        mobileList.add(new Mobile("Mi", "8GB"));
        mobileList.add(new Mobile("Samsung", "4GB"));



        ArrayList newArrayList = new ArrayList();
        for (Mobile data : mobileList) {
            if (!newArrayList.contains(data)) {
                System.out.println(data.mobile_name + ":" +data.mobile_ram);
                newArrayList.add(data);
            }

        }
    }

}
