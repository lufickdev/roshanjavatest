package com.example.javatestprograms;

class CreateSingletonClasss {

    public final static CreateSingletonClasss instanceSingleton = new CreateSingletonClasss();

    public CreateSingletonClasss() {}

    public static CreateSingletonClasss getInstance() {
        return instanceSingleton;
    }
}
