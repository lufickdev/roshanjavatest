package com.example.javatestprograms;


import java.util.Scanner;

//Question 7 Own Exception "ApiException".
class APIException extends Exception {
    public APIException(ApiError apiError) {
    }
}

enum ApiError {
    AUTH_ERROR, ACCESS_DENIED, DISK_FULL
}

public class MyException {
    public static void main(String[] args) {
        System.out.println("Enter the number between 1 to 3 :");
        Scanner myObj = new Scanner(System.in);
        int errorCode = myObj.nextInt();
        if (errorCode == 1) {
            try {
                throw new APIException(ApiError.AUTH_ERROR);
            } catch (APIException e) {
                System.out.println("Input is " + errorCode + "," + "Exception with " + ApiError.AUTH_ERROR);
            }

        } else if (errorCode == 2) {
            try {
                throw new APIException(ApiError.ACCESS_DENIED);
            } catch (APIException e) {
                System.out.println("Input is " + errorCode + "," + "Exception with " + ApiError.ACCESS_DENIED);
            }
        } else if (errorCode == 3) {
            try {
                throw new APIException(ApiError.DISK_FULL);
            } catch (APIException e) {
                System.out.println("Input is " + errorCode + "," + "Exception with " + ApiError.DISK_FULL);
            }
        } else {
            System.out.println("Oops you entered the wrong input");
            System.exit(0);
        }
    }
}
