package com.example.javatestprograms;

import android.annotation.TargetApi;
import android.os.Build;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

public class Problem {
    public static void main(String[] args) {
        Map<String, Integer> wordMap = buildWordMap("C:\\Users\\Roshan\\mobile.txt");
        List<Map.Entry<String, Integer>> list = sortByValueInDecreasingOrder(wordMap);
        System.out.println("List of repeated word from file and their count");
        for (Map.Entry<String, Integer> entry : list) {
            if (entry.getValue() >= 1) {
                System.out.println(entry.getKey() + ":" + entry.getValue());
            }
        }
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    public static Map<String, Integer> buildWordMap(String filename) {
        Map<String, Integer> wordmap = new HashMap<>();
        try (FileInputStream fis = new FileInputStream(filename);
             DataInputStream dis = new DataInputStream(fis);
             BufferedReader br = new BufferedReader(new InputStreamReader(dis))) {
            Pattern pattern = Pattern.compile("\\s+");
            String line = null;
            while ((line = br.readLine()) != null) {
//                line = line.toLowerCase();
                String[] words = pattern.split(line);
                for (String word : words
                ) {
                    if (wordmap.containsKey(word)) {
                        wordmap.put(word, (wordmap.get(word) + 1));
                    } else {
                        wordmap.put(word, 1);
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return wordmap;
    }


    public static List<Map.Entry<String, Integer>> sortByValueInDecreasingOrder(Map<String, Integer> wordmap) {
        Set<Map.Entry<String, Integer>> entries = wordmap.entrySet();
        List<Map.Entry<String, Integer>> list = new ArrayList<>(entries);
        Collections.sort(list, new Comparator<Map.Entry<String, Integer>>() {
            @Override
            public int compare(Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2) {
                return (o2.getValue()).compareTo(o1.getValue());
            }
        });
        return list;
    }
}
